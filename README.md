# Simplon Line +

## Présentation

Projet réalisé dans le cadre de la formation CDA en alternance de Simplon. 

Période 3 : Specs et UML

### Groupe de Travail :

- Chloé Tayeb (@ChloeT)
- Oriana Agniel (@Oriana.A)
- Florien Maurel (@Florian951)
- Florent Gallitre (@Flo.G)


## Brief

Avant toutes choses, nous vous invitons à consulter le [brief du projet](./brief/brief.md)

## Rendu
Notre réponse au sujet se décompose en 2 parties :
- Notre analyse du besoin : [spécifications fonctionnelles](./specifications_fonctionnelles/Spécifications_Fonctionnelles.md)
- Notre proposition technique :  [spécifications techniques](./specifications_techniques/Spécifications _Techniques.md)
