# Spécifications Fonctionnelles

## Définition des acteurs

### Membre de l'administration
> Ang: Administration member

Regroupe lea directeurice régional.e, les chargé.es de formation et les formateurice. Permet la consultation (lecture seule) d'un certain nombre d'informations administratives et d'organisation


![Diagramme de usecase: Membre de l'administration](./sources/diagrammes_use_case/Administration_member.png)


### Directeurice régional.e
> (Ang: Regional director)

Gère les différents centres de formation.

Au sein de l'application, iel pourra ajouter de nouveaux centres de formation, du nouveau personnel qu'iel pourra associer aux diférents centres.
Iel aura également pour rôle de controler les informations fournies par les chargé.es de formation au moment de monter le dossier d'une nouvelle session.


![Diagramme de usecase : directeurice régional.e](./sources/diagrammes_use_case/Regional_Director.jpg)


### Chargée de formation
> Aka: Chargé.e de promotion
> Ang: Manager

Gère la mise en place de nouvelles sessions de formation au sein du ou des centre(s) auxquels ielle est rattaché.e. Ielle doit pouvoir :
-  Créer des nouvelles promotions
- Assigner un·e formateur·ice à une promotion

- Valider une candidature
- Inviter des apprenant·e·s dans les promos


Une fois la session en cours, iel doit pouvoir effectuer le suivi:
- Gérer les retards et absences des apprenant.es
- Consulter les dossiers et projets de aprenant.es


![Diagramme de usecase : chargé.e de formation](./sources/diagrammes_use_case/Manager.jpg)


### Formateurice
> Ang: Trainer

Doit pouvoir créer des projets puis les assigner à ses promos 
Doit pouvoir corriger les projets rendus
Doit pouvoir consulter les dossiers des aprenant.es


![Diagramme de usecase : formateurice](./sources/diagrammes_use_case/Trainer.png)


### Apprenant.e
> Ang: Learner

Doit avoir accès aux projets proposés à sa promotion
Doit pouvoir proposer un rendu
Doit pouvoir consulter son avancement dans les compétences


![Diagramme de usecase : apprenant.e](./sources/diagrammes_use_case/Learner.png)


### candidat.e
> Ang: Candidate

Doit pouvoir consulter l'agenda des formations en cours ou à venir
Doit pouvoir candidater à une formation à venir

![Diagramme de usecase : candidat.e](./sources/diagrammes_use_case/Candidate.png)




## Quelques cas d'utilisation détaillées

### Chargé.e de formation : Création d'une nouvelle session de formation

#### Use case détaillé
![Use case d'étaillé : Création d'une nouvelle session de formation](./sources/use_case_detailles/nouvelle_session.png)


#### Diagramme d'activité

![Diagramme d'activité : Création d'une nouvelle session de formation](./sources/diagrammes_activite/nouvelle_session.jpg)



### Directeurice Regional.e : Validation d'une nouvelle session de formation

#### Use case détaillé

![Use case d'étaillé : Validation d'une nouvelle session de formation](./sources/use_case_detailles/valider_nouvelle_session.png)



### Chargé.e de formation : Recrutement des apprenants

#### Use case détaillé

![Use case d'étaillé : Recrutement des apprenants](./sources/use_case_detailles/recrutement.png)


#### Diagramme d'activité

![Diagramme d'activité : Recrutement des apprenants](./sources/diagrammes_activite/Recruter.jpg)



### Formateurice : création d'un nouveau projet

#### Use case détaillé

![Use case d'étaillé : création d'un nouveau projet](./sources/use_case_detailles/creation_projet.png)


#### Diagramme d'activité
![Diagramme d'activité : création d'un nouveau projet](./sources/diagrammes_activite/creation_projet.png)



### Aprenante : rendu d'un projet

#### Use case détaillé

![Use case d'étaillé : rendu d'un projet](./sources/use_case_detailles/rendu_projet.png)


#### Diagramme d'activité

![Diagramme d'activité : rendu d'un projet](./sources/diagrammes_activite/rendu.png)


### Aprenante : signaler un retard ou une absence

#### Use case détaillé

![Use case d'étaillé : signaler un retard ou une absence](./sources/use_case_detailles/signaler_retard.png)


#### Diagramme d'activité

![Diagramme d'activité : signaler un retard ou une absence](./sources/diagrammes_activite/signaler_retard.png)



### Chargé.e de formation : Validation du motif de retard ou d'absence

#### Use case détaillé

![Use case d'étaillé : Validation du motif de retard ou d'absence](./sources/use_case_detailles/valider_motif.png)




## Maquette fonctionnelle

### Recrutement des candidats

Vous pouvez consulter notre [Maquette](https://marvelapp.com/prototype/691f08h/screen/72160659) réalisée avec Marvel App





> Retour au [Readme](../README.md)
> Passer aux [specifications Techniques](../specifications_techniques/Spécifications_Techniques.md)