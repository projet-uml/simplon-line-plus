# Spécifications Techniques



## Choix  technologiques

| Besoin                                                       | Solutions                                                    | Technologies envisagées |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ----------------------- |
| Une plateforme unifiée pour regrouper les différentes tâches courantes propres à chaque centre. | Développement d'une application web accessible par les directeurices de régions, les chargé·e·s de promos,formateur·ice·s, les apprenant-e-s et les candidat-e-s | Symfony et Doctrine     |



## Diagramme de classes

### Business

![Diagramme de classes](.sources/diagrammes_classes/business.jpg)


### Entités


![Diagramme d'entités](.sources/diagrammes_classes/entites.jpg)



## Diagrammes de séquences

### Création et assignation d'un projet

![Diagramme de séquence : Création et assignation d'un projet](./sources/diagramme_sequences/creation_projet.png)

### Rendu d'un projet

![Diagramme de séquence : Rendu d'un projet](./sources/diagramme_sequences/ajout_de_rendu.png)


### Validation d'une candidature

![Diagramme de séquence : Validation d'une candidature](./sources/diagramme_sequences/validation_candidature.jpg)


### Signaler un retard

![Diagramme de séquence : Signaler un retard](./sources/diagramme_sequences/signaler_retard.png)


### Consulter une formation à valider

![Diagramme de séquence : Valider une formation](./sources/diagramme_sequences/consulter_formation_a_valider.jpg)






> Retour au [Readme](../README.md)
> Retour aux [specifications fonctionelles](../specifications_fonctionnelles/Spécifications_Fonctionnelles.md)